# ICRAFT TUTORIAL

本仓库主要介绍基于icraft进行模型部署的使用方法。

3.1.1版本之前只有运行时api的使用及相关demo介绍。

3.1.1版本开始对tutorial进行了细分，包括：快速开始，icraft编译用法，运行时api用法，计时相关，profiler相关用法等。

3.7.1版本的tutorial则进一步扩充了:icraft性能分析及优化教程、icraft硬算子教程、问题自助排查教程、FAQ等内容。

**版本分支指引：**

- [icraft_2.2.0](https://gitee.com/mxh-spiger/tutorial-runtime/tree/rt2.2.0/)
- [icraft_3.0.0](https://gitee.com/mxh-spiger/tutorial-runtime/tree/rt3.0.0/)
- [icraft_3.1.1](https://gitee.com/mxh-spiger/tutorial-runtime/tree/tt3.1.1/)
- [icraft_3.6.2](https://gitee.com/mxh-spiger/tutorial-runtime/tree/tt3.6.2/)
- [icraft_3.7.1](https://gitee.com/mxh-spiger/tutorial-runtime/tree/tt3.7.1/)

